package com.company;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {
    private static String FILE = "FirstPdf.pdf";
    private static Font font = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static final String NAME = "Adam";
    private static final String LAST_NAME = "Kulas";
    private static final String PROFESSION = "Student";
    private static final String EDUCATION = "2014 - 2018: ZSP Ryglice \n"
            + "2018 - now: PWSZ Tarnów";
    private static final String SUMMARY = "Anything ";

    public static void main(String[] args) {
        try {
            Document document = new Document();
            createPdf(document);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createPdf(Document document) throws DocumentException, FileNotFoundException {
        PdfWriter.getInstance(document, new FileOutputStream(FILE));
        document.open();
        addContent(document);
        document.close();
    }

    private static void addContent(Document document) throws DocumentException {
        createHeader(document);
        createTable(document);
    }

    private static void createHeader(Document document) throws DocumentException {
        Paragraph paragraph = new Paragraph("RESUME", font);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.setSpacingAfter(50);
        document.add(paragraph);
    }

    private static void createTable(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        table.getDefaultCell().setPadding(20);
        table.addCell("First Name");
        table.addCell(NAME);
        table.addCell("Last Name");
        table.addCell(LAST_NAME);
        table.addCell("Profession");
        table.addCell(PROFESSION);
        table.addCell("Education");
        table.addCell(EDUCATION);
        table.addCell("Summary");
        table.addCell(SUMMARY);
        document.add(table);
    }

}